#!/bin/bash

clear

echo What is your name?

read -r MYNAME

echo What is a fun fact about you?

read -r FUNFACT

echo "I am $MYNAME. A fun fact about me is: $FUNFACT"