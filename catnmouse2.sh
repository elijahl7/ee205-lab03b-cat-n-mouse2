#!/bin/bash

clear

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

echo "The max value is $THE_MAX_VALUE"

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

while
  echo "OK cat, I’m thinking of a number from 1 to $THE_MAX_VALUE. Make a guess:"
  read -r GUESS


  if [ "$GUESS" -lt 1 ]
  then
    echo "You must enter a number that’s >= 1"
  elif [ "$GUESS" -gt $THE_MAX_VALUE ]
  then
    echo "You must enter a number that’s <= $THE_MAX_VALUE"
  elif [ "$GUESS" -gt $THE_NUMBER_IM_THINKING_OF ]
  then
    echo "No cat... the number I’m thinking of is smaller than $GUESS"
  elif [ "$GUESS" -lt $THE_NUMBER_IM_THINKING_OF ]
  then
    echo "No cat... the number I’m thinking of is larger than $GUESS"
  fi

  [[ $GUESS -ne $THE_NUMBER_IM_THINKING_OF ]]
do true; done

echo "You got me."

echo "|\---/|"
echo "| o_o |"
echo " \_^_/"